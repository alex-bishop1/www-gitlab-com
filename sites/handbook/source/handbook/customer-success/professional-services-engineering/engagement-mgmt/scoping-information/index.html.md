---
layout: handbook-page-toc
title: "Professional Services EM Scoping Guidelines"
description: "Describes the process of scoping PS engagements."

---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

:warning: This page is under construction 

# Scoping Information

We use the [Engagement Estimate TEMPLATES](https://docs.google.com/spreadsheets/d/1YKMyflzsA-VPEVobB82zC8-n0hlC-uRBtiNB7Fm-kZg/edit?usp=sharing) workbook as a starting point for determining the activities, duration, and price of a Professional Services opportunity.

## Scoping Specific Types of Services

See the detailed notes pages for each service type below.

- [GitLab implementation scoping](/handbook/customer-success/professional-services-engineering/engagement-mgmt/scoping-information/implementation/)
- [GitLab Readiness Assessment (Health Check)](/handbook/customer-success/professional-services-engineering/engagement-mgmt/scoping-information/readiness/)
- [GitLab migration scoping - GitLab, Bitbucket Server, or GitHub (Enterprise or .com) -> GitLab](/handbook/customer-success/professional-services-engineering/engagement-mgmt/scoping-information/migrations/)
- [CI pipeline migration scoping](/handbook/customer-success/professional-services-engineering/engagement-mgmt/scoping-information/ci-pipeline/)
- [Transformational Services scoping](/handbook/customer-success/professional-services-engineering/engagement-mgmt/scoping-information/transformational-services/)

